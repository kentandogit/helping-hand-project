package me.helpinghand.helpinghand.bootstrap;

import me.helpinghand.helpinghand.models.Borrower;
import me.helpinghand.helpinghand.models.Lender;
import me.helpinghand.helpinghand.models.Role;
import me.helpinghand.helpinghand.models.User;
import me.helpinghand.helpinghand.repositories.BorrowerRepository;
import me.helpinghand.helpinghand.repositories.LenderRepository;
import me.helpinghand.helpinghand.repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class BootstrapData implements CommandLineRunner {

    private final UserRepository userRepository;
    private final LenderRepository lenderRepository;
    private final BorrowerRepository borrowerRepository;
    private final PasswordEncoder passwordEncoder;

    public BootstrapData(UserRepository userRepository,
                         LenderRepository lenderRepository,
                         BorrowerRepository borrowerRepository,
                         PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.lenderRepository = lenderRepository;
        this.borrowerRepository = borrowerRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {

        User admin = userRepository.findByEmail("admin@admin.com").orElse(new User());
        if(admin.getId() == null) {
            createInitialUsers();
        }
    }

    private void createInitialUsers() {
        User admin = new User();
        admin.setEmail("admin@admin.com");
        admin.setRole(Role.ADMIN);
        admin.setFirstName("Admin");
        admin.setLastName("Admin");
        admin.setPassword(passwordEncoder.encode("password"));

        userRepository.save(admin);

        User lenderUser = new User();
        lenderUser.setEmail("lender@lender.com");
        lenderUser.setRole(Role.LENDER);
        lenderUser.setFirstName("Lender");
        lenderUser.setLastName("Lender");
        lenderUser.setPassword(passwordEncoder.encode("password"));

        Lender lender = new Lender();
        lender.setAddress("Somewhere around");
        lender.setCompanyName("Just a Lender");
        lender.setDescription("Just a simple lending company");
        lender.setContactNumber("+651234567");
        lender.setUser(lenderUser);

        lenderRepository.save(lender);

        User borrowerUser = new User();
        borrowerUser.setEmail("borrower@borrower.com");
        borrowerUser.setRole(Role.BORROWER);
        borrowerUser.setFirstName("Borrower");
        borrowerUser.setLastName("Borrower");
        borrowerUser.setPassword(passwordEncoder.encode("password"));

        Borrower borrower = new Borrower();
        borrower.setAddress("Somewhere around");
        borrower.setDescription("Just a simple borrower person");
        borrower.setContactNumber("+651234567");
        borrower.setAssets(BigDecimal.valueOf(200.50));
        borrower.setLiabilities(BigDecimal.valueOf(50.50));
        borrower.setIsSelfEmployed(true);
        borrower.setIsEmployed(true);
        borrower.setUser(borrowerUser);

        borrowerRepository.save(borrower);
    }
}
