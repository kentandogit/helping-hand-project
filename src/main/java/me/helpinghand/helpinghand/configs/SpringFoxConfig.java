package me.helpinghand.helpinghand.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
@Import({BeanValidatorPluginsConfiguration.class})
public class SpringFoxConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build().apiInfo(apiInfo())
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Collections.singletonList(apiKey()));
    }

    private ApiKey apiKey() {
        return new ApiKey("JWT", "Authorization", "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Collections.singletonList(new SecurityReference("JWT", authorizationScopes));
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Helping Hand Rest API",
                "### REST Endpoints for Helping Hand.\n" +
                        "1. The System has 3 types of users - **ADMIN**, **LENDER**, and **BORROWERS**.\n" +
                        "2. Admins can list all borrowers, lenders, and users as well as list all loan applications and search for loans.\n" +
                        "3. Lenders can create loans.\n" +
                        "4. Borrowers can search for loans and apply for them.\n" +
                        "5. Lenders can then search for all Loan Applications in their Loans and approved or reject them.\n" +
                        "### Sample Users:\n" +
                        "1. Admin - admin@admin.com/password\n" +
                        "2. Borrower - borrower@borrower.com/password\n" +
                        "3. Lender - lender@lender.com/password\n",
                "v1",
                "Terms of service",
                new Contact("Kent Allan Jay Ando", "https://www.kentando.me", "kentando@gmial.com"),
                "Apache License Version 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0",
                Collections.emptyList());
    }
}