package me.helpinghand.helpinghand.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class LenderRequest extends UserRequest{
    private String companyName;
    private String address;
    private String contactNumber;
    private String description;
}
