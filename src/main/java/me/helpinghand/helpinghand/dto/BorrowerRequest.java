package me.helpinghand.helpinghand.dto;

import lombok.*;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class BorrowerRequest extends UserRequest {
    private BigDecimal assets;
    private BigDecimal liabilities;
    private Boolean isEmployed;
    private Boolean isSelfEmployed;
    private String address;
    private String contactNumber;
    private String description;
}
