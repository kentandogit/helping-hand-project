package me.helpinghand.helpinghand.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {
    private String password;
    private String firstName;
    private String lastName;
    private String email;
}
