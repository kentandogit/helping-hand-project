package me.helpinghand.helpinghand.models;

public enum ApplicationStatus {
    PENDING, REVIEWING, APPROVED, REJECTED
}
