package me.helpinghand.helpinghand.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
public class Lender {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String companyName;

    @NotEmpty
    @Lob
    private String address;

    @NotEmpty
    private String contactNumber;

    @NotEmpty
    @Lob
    private String description;

    // showcasing different options
    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(
            name = "user_id",
            unique = true,
            nullable = false,
            updatable = false
    )
    @ToString.Exclude
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private User user;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "lender")
    private Set<Loan> loans = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Lender lender = (Lender) o;
        return Objects.equals(id, lender.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
