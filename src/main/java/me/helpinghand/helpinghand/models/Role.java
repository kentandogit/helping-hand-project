package me.helpinghand.helpinghand.models;

public enum Role {
    ADMIN, LENDER, BORROWER;
}
