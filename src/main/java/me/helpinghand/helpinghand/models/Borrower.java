package me.helpinghand.helpinghand.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
public class Borrower {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal assets;

    @NotNull
    private BigDecimal liabilities;

    @NotNull
    private Boolean isEmployed;

    @NotNull
    private Boolean isSelfEmployed;

    @NotEmpty
    @Lob
    private String address;

    @NotEmpty
    private String contactNumber;

    @NotEmpty
    @Lob
    private String description;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(
            name = "user_id",
            unique = true,
            nullable = false,
            updatable = false
    )
    private User user;

    @OneToMany(mappedBy = "borrower")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Set<LoanApplication> loanApplications;

    public BigDecimal getNetWorth()
    {
        return assets.subtract(liabilities);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Borrower borrower = (Borrower) o;
        return Objects.equals(id, borrower.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
