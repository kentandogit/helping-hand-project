package me.helpinghand.helpinghand.models;

import lombok.*;
import org.hibernate.Hibernate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Objects;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
public class LoanApplication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne
    private Loan loan;

    @NotNull
    @ManyToOne
    private Borrower borrower;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ApplicationStatus applicationStatus;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate applicationDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate approvedDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        LoanApplication loan = (LoanApplication) o;
        return Objects.equals(id, loan.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
