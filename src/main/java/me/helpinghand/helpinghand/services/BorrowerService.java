package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.dto.BorrowerRequest;
import me.helpinghand.helpinghand.dto.LenderRequest;
import me.helpinghand.helpinghand.models.Borrower;
import me.helpinghand.helpinghand.models.Lender;

import java.util.Set;

public interface BorrowerService {

    Borrower save(BorrowerRequest borrowerRequest);
    Borrower update(BorrowerRequest borrowerRequest, Long id);
    Set<Borrower> findAll();
}
