package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.dto.BorrowerRequest;
import me.helpinghand.helpinghand.models.Borrower;
import me.helpinghand.helpinghand.models.Role;
import me.helpinghand.helpinghand.models.User;
import me.helpinghand.helpinghand.repositories.BorrowerRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class BorrowerServiceImpl extends UserBaseService implements BorrowerService {

    private final BorrowerRepository borrowerRepository;

    public BorrowerServiceImpl(BorrowerRepository borrowerRepository, PasswordEncoder passwordEncoder) {
        super(passwordEncoder);
        this.borrowerRepository = borrowerRepository;
    }

    @Override
    public Borrower save(BorrowerRequest borrowerRequest) {
        User borrowerUser = setUserFromRequest(borrowerRequest, new User());
        borrowerUser.setEmail(borrowerRequest.getEmail());
        borrowerUser.setRole(Role.BORROWER);

        Borrower borrower = new Borrower();
        borrower.setUser(borrowerUser);
        borrower.setAssets(borrowerRequest.getAssets());
        borrower.setLiabilities(borrowerRequest.getLiabilities());
        borrower.setIsEmployed(borrowerRequest.getIsEmployed());
        borrower.setIsSelfEmployed(borrowerRequest.getIsSelfEmployed());
        borrower.setDescription(borrowerRequest.getDescription());
        borrower.setAddress(borrowerRequest.getAddress());
        borrower.setContactNumber(borrowerRequest.getContactNumber());

        return borrowerRepository.save(borrower);
    }

    @Override
    public Borrower update(BorrowerRequest borrowerRequest, Long id) {
        Borrower borrower = borrowerRepository.findById(id).orElseThrow(() -> new RuntimeException("Borrower does not exists"));

        borrower.setUser(setUserFromRequest(borrowerRequest, borrower.getUser()));

        if(borrowerRequest.getAssets() != null) {
            borrower.setAssets(borrowerRequest.getAssets());
        }

        if(borrowerRequest.getLiabilities() != null) {
            borrower.setLiabilities(borrowerRequest.getLiabilities());
        }

        if(borrowerRequest.getIsEmployed() != null) {
            borrower.setIsEmployed(borrowerRequest.getIsEmployed());
        }

        if(borrowerRequest.getIsSelfEmployed() != null) {
            borrower.setIsSelfEmployed(borrowerRequest.getIsSelfEmployed());
        }

        if(borrowerRequest.getDescription() != null) {
            borrower.setDescription(borrowerRequest.getDescription());
        }
        if(borrowerRequest.getAddress() != null) {
            borrower.setAddress(borrowerRequest.getAddress());
        }
        if(borrowerRequest.getContactNumber() != null) {
            borrower.setContactNumber(borrowerRequest.getContactNumber());
        }

        return borrowerRepository.save(borrower);
    }

    @Override
    public Set<Borrower> findAll() {
        return new HashSet<>(borrowerRepository.findAll());
    }
}
