package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.models.Loan;
import org.springframework.security.core.Authentication;

import java.math.BigDecimal;
import java.util.Set;

public interface LoanService {

    Loan save(Loan loan, String username);
    Loan update(Loan loan);
    Boolean lenderOwnsLoan(Long loanId, String username);
    Set<Loan> getLenderLoans(String username);
    Set<Loan> searchLoan(String name, String description, String requirements, BigDecimal maxTakeOut, String username);
}
