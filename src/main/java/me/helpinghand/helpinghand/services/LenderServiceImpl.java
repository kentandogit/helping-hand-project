package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.dto.LenderRequest;
import me.helpinghand.helpinghand.models.Lender;
import me.helpinghand.helpinghand.models.Role;
import me.helpinghand.helpinghand.models.User;
import me.helpinghand.helpinghand.repositories.LenderRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class LenderServiceImpl extends UserBaseService implements LenderService {

    private final LenderRepository lenderRepository;

    public LenderServiceImpl(LenderRepository lenderRepository, PasswordEncoder passwordEncoder) {
        super(passwordEncoder);
        this.lenderRepository = lenderRepository;
    }

    @Override
    public Lender save(LenderRequest lenderRequest) {

        User lenderUser = setUserFromRequest(lenderRequest, new User());
        lenderUser.setEmail(lenderRequest.getEmail());
        lenderUser.setRole(Role.LENDER);

        Lender lender = new Lender();
        lender.setUser(lenderUser);
        lender.setDescription(lenderRequest.getDescription());
        lender.setContactNumber(lenderRequest.getContactNumber());
        lender.setAddress(lenderRequest.getAddress());
        lender.setCompanyName(lenderRequest.getCompanyName());

        return lenderRepository.save(lender);
    }

    @Override
    public Lender update(LenderRequest lenderRequest, Long id) {

        Lender lender = lenderRepository.findById(id).orElseThrow(() -> new RuntimeException("Lender does not exists"));
        lender.setUser(setUserFromRequest(lenderRequest, lender.getUser()));

        if(lenderRequest.getDescription() != null) {
            lender.setDescription(lenderRequest.getDescription());
        }

        if(lenderRequest.getContactNumber() != null) {
            lender.setContactNumber(lenderRequest.getContactNumber());
        }

        if(lenderRequest.getAddress() != null) {
            lender.setAddress(lenderRequest.getAddress());
        }

        if(lenderRequest.getCompanyName() != null) {
            lender.setCompanyName(lenderRequest.getCompanyName());
        }

        return lenderRepository.save(lender);
    }

    @Override
    public Set<Lender> findAll() {
        return new HashSet<>(lenderRepository.findAll());
    }
}
