package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.models.ApplicationStatus;
import me.helpinghand.helpinghand.models.Loan;
import me.helpinghand.helpinghand.models.LoanApplication;
import me.helpinghand.helpinghand.models.User;
import me.helpinghand.helpinghand.repositories.LoanApplicationRepository;
import me.helpinghand.helpinghand.repositories.LoanRepository;
import me.helpinghand.helpinghand.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Set;

@Service
public class LoanApplicationServiceImpl implements LoanApplicationService {

    private final UserRepository userRepository;
    private final LoanRepository loanRepository;
    private final LoanApplicationRepository loanApplicationRepository;

    public LoanApplicationServiceImpl(UserRepository userRepository,
                                      LoanRepository loanRepository,
                                      LoanApplicationRepository loanApplicationRepository) {
        this.userRepository = userRepository;
        this.loanRepository = loanRepository;
        this.loanApplicationRepository = loanApplicationRepository;
    }

    @Override
    public Set<LoanApplication> getApplications(Long loanId) {
        return loanApplicationRepository.getByLoanId(loanId);
    }

    @Override
    public LoanApplication apply(Long loanId, String username) {

        User user = userRepository.findByEmail(username).orElseThrow(() -> new RuntimeException("User not found"));
        Loan loan = loanRepository.findById(loanId).orElseThrow(() -> new RuntimeException("Loan not found"));

        if(loan.getAvailableDate().isAfter(LocalDate.now())) {
            throw new RuntimeException("Loan is not available");
        }

        // @todo maybe validation logic here to make sure borrower fits the loan's criteria

        LoanApplication la = new LoanApplication();
        la.setBorrower(user.getBorrower());
        la.setLoan(loan);
        la.setApplicationStatus(ApplicationStatus.PENDING);
        la.setApplicationDate(LocalDate.now());
        return loanApplicationRepository.save(la);
    }

    @Override
    public LoanApplication updateStatus(Long applicationId, ApplicationStatus applicationStatus) {

        LoanApplication loanApplication = loanApplicationRepository
                .findById(applicationId).orElseThrow(() -> new RuntimeException("Loan application not found"));

        switch (applicationStatus) {
            case APPROVED:
                loanApplication.setApprovedDate(LocalDate.now());
            default:
                loanApplication.setApplicationStatus(applicationStatus);
        }

        return loanApplicationRepository.save(loanApplication);
    }

    @Override
    public Boolean lenderOwnedApplication(Long applicationId, String username) {

        User user = userRepository.findByEmail(username).orElseThrow(() -> new RuntimeException("User not found"));
        LoanApplication loanApplication = loanApplicationRepository
                .findById(applicationId).orElseThrow(() -> new RuntimeException("Loan application not found"));

        return user.getLender().getId().equals(loanApplication.getLoan().getLender().getId());
    }

    @Override
    public Boolean borrowerAlreadyApplied(Long loanId, String username) {
        User user = userRepository.findByEmail(username).orElseThrow(() -> new RuntimeException("User not found"));
        return user.getBorrower().getLoanApplications()
                .stream().anyMatch((loanApplication -> loanApplication.getLoan().getId().equals(loanId)));
    }
}
