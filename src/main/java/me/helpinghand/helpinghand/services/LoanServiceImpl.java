package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.models.Loan;
import me.helpinghand.helpinghand.models.Role;
import me.helpinghand.helpinghand.models.User;
import me.helpinghand.helpinghand.repositories.LoanRepository;
import me.helpinghand.helpinghand.repositories.UserRepository;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Set;

@Service
public class LoanServiceImpl implements LoanService {

    private final LoanRepository loanRepository;
    private final UserRepository userRepository;

    public LoanServiceImpl(LoanRepository loanRepository, UserRepository userRepository) {
        this.loanRepository = loanRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Loan save(Loan loan, String username) {
        User user = userRepository.findByEmail(username).orElseThrow(() -> new RuntimeException("User not found"));
        loan.setLender(user.getLender());

        return loanRepository.save(loan);
    }

    @Override
    public Loan update(Loan loan) {
        Loan existingLoan = loanRepository.findById(loan.getId()).orElseThrow(() -> new RuntimeException("Loan not found"));

        // Essentially similar to MapStruct
        if(loan.getName() != null) {
            existingLoan.setName(loan.getName());
        }

        if(loan.getDescription() != null) {
            existingLoan.setDescription(loan.getDescription());
        }

        if(loan.getRequirements() != null) {
            existingLoan.setRequirements(loan.getRequirements());
        }

        if(loan.getMinNetWorth() != null) {
            existingLoan.setMinNetWorth(loan.getMinNetWorth());
        }

        if(loan.getAllowSelfEmployed() != null) {
            existingLoan.setAllowSelfEmployed(loan.getAllowSelfEmployed());
        }

        if(loan.getAvailableDate() != null) {
            existingLoan.setAvailableDate(loan.getAvailableDate());
        }

        if(loan.getPublished() != null) {
            existingLoan.setPublished(loan.getPublished());
        }
        return loanRepository.save(existingLoan);
    }

    @Override
    public Boolean lenderOwnsLoan(Long loanId, String username) {
        User user = userRepository.findByEmail(username).orElseThrow(() -> new RuntimeException("User not found"));
        Loan loan = loanRepository.findById(loanId).orElseThrow(() -> new RuntimeException("Loan not found"));

        return loan.getLender().getId().equals(user.getLender().getId());
    }

    @Override
    public Set<Loan> getLenderLoans(String username) {
        User user = userRepository.findByEmail(username).orElseThrow(() -> new RuntimeException("User not found"));

        return user.getLender().getLoans();
    }

    @Override
    public Set<Loan> searchLoan(String name,
                                String description,
                                String requirements,
                                BigDecimal maxTakeOut,
                                String username) {

        // we must always know who is searching
        User user = userRepository.findByEmail(username).orElseThrow(() -> new RuntimeException("User not found"));

        // extra check
        if(user.getRole() == Role.LENDER) {
            throw new AccessDeniedException("User is not allowed to do this");
        }

        BigDecimal netWorth = null;

        if(user.getRole() == Role.BORROWER) {
            netWorth = user.getBorrower().getNetWorth();
        }

        return loanRepository.searchLoans(name, description, requirements, maxTakeOut, netWorth);
    }
}
