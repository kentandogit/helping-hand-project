package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.dto.UserRequest;
import me.helpinghand.helpinghand.models.User;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserBaseService {

    private final PasswordEncoder passwordEncoder;

    public UserBaseService(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    protected User setUserFromRequest(UserRequest userRequest, User user) {
        if(userRequest.getLastName() != null) {
            user.setLastName(userRequest.getLastName());
        }

        if(userRequest.getFirstName() != null) {
            user.setFirstName(userRequest.getFirstName());
        }

        if(userRequest.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        }

        return user;
    }
}
