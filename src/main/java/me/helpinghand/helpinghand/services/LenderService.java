package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.dto.LenderRequest;
import me.helpinghand.helpinghand.models.Lender;

import java.util.Set;

public interface LenderService {

    Lender save(LenderRequest lenderRequest);
    Lender update(LenderRequest lenderRequest, Long id);
    Set<Lender> findAll();
}
