package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.models.ApplicationStatus;
import me.helpinghand.helpinghand.models.LoanApplication;

import java.util.Set;

public interface LoanApplicationService {

    Set<LoanApplication> getApplications(Long loanId);
    LoanApplication apply(Long loanId, String username);
    LoanApplication updateStatus(Long applicationId, ApplicationStatus applicationStatus);
    Boolean lenderOwnedApplication(Long applicationId, String username);
    Boolean borrowerAlreadyApplied(Long loanId, String username);
}
