package me.helpinghand.helpinghand.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.helpinghand.helpinghand.dto.BorrowerRequest;
import me.helpinghand.helpinghand.models.Borrower;
import me.helpinghand.helpinghand.services.BorrowerService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@Api(tags = "Borrowers")
@RestController
@RequestMapping("/api/v1/admin/borrowers")
public class AdminBorrowerController {

    private final BorrowerService borrowerService;

    public AdminBorrowerController(BorrowerService borrowerService) {
        this.borrowerService = borrowerService;
    }

    @ApiOperation(value = "GET All Borrowers", notes = "List all borrowers in the system")
    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public Set<Borrower> getAllBorrowers() {
        return borrowerService.findAll();
    }

    @ApiOperation(value = "Create Borrower", notes = "Creates a borrower")
    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public Borrower createBorrower(@RequestBody BorrowerRequest request) {
        return borrowerService.save(request);
    }

    @ApiOperation(value = "Update Borrower", notes = "Updates a borrower")
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Borrower updateBorrower(@RequestBody BorrowerRequest request, @PathVariable Long id) {
        return borrowerService.update(request, id);
    }
}
