package me.helpinghand.helpinghand.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.helpinghand.helpinghand.security.JwtRequest;
import me.helpinghand.helpinghand.security.JwtResponse;
import me.helpinghand.helpinghand.security.JwtUtility;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = " Login")
@RestController
public class Authenticate {

    private final JwtUtility jwtUtility;
    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;

    public Authenticate(JwtUtility jwtUtility, AuthenticationManager authenticationManager, UserDetailsService userDetailsService) {
        this.jwtUtility = jwtUtility;
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
    }

    @ApiOperation(value = "Login", notes = "Use to get JWT from the system using a valid email/password")
    @PostMapping("/api/v1/authenticate")
    public JwtResponse authenticate(@RequestBody JwtRequest jwtRequest) throws Exception {

        // try to login
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            jwtRequest.getUsername(), jwtRequest.getPassword()
                    )
            );
        } catch (BadCredentialsException | InternalAuthenticationServiceException e) {
            throw new BadCredentialsException("Invalid Credentials", e);
        }

        // create userDetails and generate token
        UserDetails userDetails = userDetailsService.loadUserByUsername(jwtRequest.getUsername());
        String token = jwtUtility.generateToken(userDetails);

        // respond access_token
        return new JwtResponse(token);
    }
}
