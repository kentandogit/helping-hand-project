package me.helpinghand.helpinghand.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.helpinghand.helpinghand.models.ApplicationStatus;
import me.helpinghand.helpinghand.models.LoanApplication;
import me.helpinghand.helpinghand.services.LoanApplicationService;
import me.helpinghand.helpinghand.services.LoanService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@Api(tags = "Loan Application")
@RestController
@RequestMapping("/api/v1/loans/{loanId}")
public class LoanApplicationController {

    private final LoanApplicationService loanApplicationService;
    private final LoanService loanService;

    public LoanApplicationController(LoanApplicationService loanApplicationService, LoanService loanService) {
        this.loanApplicationService = loanApplicationService;
        this.loanService = loanService;
    }

    @ApiOperation(value = "GET Loan Applications", notes = "List all applications in a given loan")
    @GetMapping("/applications")
    @PreAuthorize("hasAnyAuthority('LENDER', 'ADMIN')")
    public Set<LoanApplication> getLoanApplications(@PathVariable Long loanId, Authentication authentication) {
        if((authentication.getAuthorities().contains(new SimpleGrantedAuthority("ADMIN"))) ||
            (authentication.getAuthorities().contains(new SimpleGrantedAuthority("LENDER"))
                    && loanService.lenderOwnsLoan(loanId, authentication.getName()))
        ) {
            return loanApplicationService.getApplications(loanId);
        }
        throw new RuntimeException("User not authorized");
    }

    // this will create a DB record should this be a POST?
    // but this request dont have a BODY... hmmm
    @ApiOperation(value = "Apply for Loan", notes = "Borrowers applying for a loan")
    @GetMapping("/apply")
    @PreAuthorize("hasAuthority('BORROWER')")
    public LoanApplication apply(@PathVariable Long loanId, Authentication authentication) {
        if(!loanApplicationService.borrowerAlreadyApplied(loanId, authentication.getName())) {
            return loanApplicationService.apply(loanId, authentication.getName());
        }
        throw new RuntimeException("Borrower already applied");
    }

    @ApiOperation(value = "Application to Reviewing", notes = "Set Loan Application status to Reviewing")
    @GetMapping("/applications/{applicationId}/reviewing")
    @PreAuthorize("hasAuthority('LENDER')")
    public LoanApplication setReviewing(@PathVariable Long applicationId, Authentication authentication) {
        if(loanApplicationService.lenderOwnedApplication(applicationId, authentication.getName())) {
            return loanApplicationService.updateStatus(applicationId, ApplicationStatus.REVIEWING);
        }
        throw new RuntimeException("User not authorized to do this");
    }

    @ApiOperation(value = "Application to Approved", notes = "Set Loan Application status to Approved")
    @GetMapping("/applications/{applicationId}/approved")
    @PreAuthorize("hasAuthority('LENDER')")
    public LoanApplication setApproved(@PathVariable Long applicationId, Authentication authentication) {
        if(loanApplicationService.lenderOwnedApplication(applicationId, authentication.getName())) {
            return loanApplicationService.updateStatus(applicationId, ApplicationStatus.APPROVED);
        }
        throw new RuntimeException("User not authorized to do this");
    }

    @ApiOperation(value = "Application to Rejected", notes = "Set Loan Application status to Rejected")
    @GetMapping("/applications/{applicationId}/rejected")
    @PreAuthorize("hasAuthority('LENDER')")
    public LoanApplication setRejected(@PathVariable Long applicationId, Authentication authentication) {
        if(loanApplicationService.lenderOwnedApplication(applicationId, authentication.getName())) {
            return loanApplicationService.updateStatus(applicationId, ApplicationStatus.REJECTED);
        }
        throw new RuntimeException("User not authorized to do this");
    }

}
