package me.helpinghand.helpinghand.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.helpinghand.helpinghand.models.User;
import me.helpinghand.helpinghand.repositories.UserRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api/v1/users")
@Api(tags = "Users")
public class UserController {
    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @ApiOperation(value = "GET All Users", notes = "List all users in the system")
    @GetMapping("/all")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Set<User> all() {
        return new HashSet<User>(userRepository.findAll());
    }
}
