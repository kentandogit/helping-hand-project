package me.helpinghand.helpinghand.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.helpinghand.helpinghand.models.Loan;
import me.helpinghand.helpinghand.services.LoanService;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Set;

@Api(tags = "Loans")
@RestController
@RequestMapping("/api/v1/loans")
public class LoanController {

    private final LoanService loanService;

    public LoanController(LoanService loanService) {
        this.loanService = loanService;
    }

    @InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    @ApiOperation(value = "GET Loans", notes = "List All Loans owed by the Lender")
    @GetMapping
    @PreAuthorize("hasAuthority('LENDER')")
    public Set<Loan> getLenderLoans(Authentication authentication) {

        return loanService.getLenderLoans(authentication.getName());

    }

    @ApiOperation(value = "Create Loan", notes = "Lender creates a Loan")
    @PostMapping
    @PreAuthorize("hasAuthority('LENDER')")
    public Loan createLoan(@RequestBody Loan loan, Authentication authentication) {
        return loanService.save(loan, authentication.getName());
    }

    @ApiOperation(value = "Update Loan", notes = "Lender updates a Loan")
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('LENDER')")
    public Loan updateLoan(@RequestBody Loan loan, @PathVariable Long id, Authentication authentication) {
        if(loanService.lenderOwnsLoan(id, authentication.getName())) {
            // even if id gets binded it will still be overwritten here?
            loan.setId(id);
            return loanService.update(loan);
        }
        throw new AccessDeniedException("User is not authorize to do this");
    }

    @ApiOperation(value = "Search Loan", notes = "Borrower or Admin search for a loan")
    @GetMapping("/search")
    @PreAuthorize("hasAuthority('BORROWER') || hasAuthority('ADMIN')")
    public Set<Loan> searchLoans(
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "requirements", required = false) String requirements,
            @RequestParam(value = "maxtakeout", required = false) BigDecimal maxTakeOut,
            Authentication authentication
            ) {
        return loanService.searchLoan(name, description, requirements, maxTakeOut, authentication.getName());
    }
}
