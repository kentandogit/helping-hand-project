package me.helpinghand.helpinghand.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.helpinghand.helpinghand.dto.LenderRequest;
import me.helpinghand.helpinghand.models.Lender;
import me.helpinghand.helpinghand.services.LenderService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@Api(tags = "Lenders")
@RestController
@RequestMapping("/api/v1/admin/lenders")
public class AdminLenderController {

    private final LenderService lenderService;

    public AdminLenderController(LenderService lenderService) {
        this.lenderService = lenderService;
    }

    @ApiOperation(value = "GET All Lenders", notes = "List all the Lenders in the system")
    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public Set<Lender> getAllLenders() {
        return lenderService.findAll();
    }

    @ApiOperation(value = "Create Lender", notes = "Creates a Lender")
    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public Lender createLender(@RequestBody LenderRequest request) {
        return lenderService.save(request);
    }

    @ApiOperation(value = "Update Lender", notes = "Updates a Lender")
    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Lender updateLender(@RequestBody LenderRequest request, @PathVariable Long id) {
        return lenderService.update(request, id);
    }
}
