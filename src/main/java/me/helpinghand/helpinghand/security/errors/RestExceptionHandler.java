package me.helpinghand.helpinghand.security.errors;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.*;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Handle HttpMessageNotReadableException. Happens when request JSON is malformed.
     *
     * @param ex      HttpMessageNotReadableException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        String error = "Malformed JSON request";

        return buildResponseEntity(new RestErrorResponse(HttpStatus.BAD_REQUEST, error, ex));
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        RestErrorResponse restErrorResponse = new RestErrorResponse(METHOD_NOT_ALLOWED);
        restErrorResponse.setMessage(ex.getMessage());

        return buildResponseEntity(restErrorResponse);
    }

    /**
     * Handles javax.validation.ConstraintViolationException. Thrown when @Validated fails.
     *
     * @param ex the ConstraintViolationException
     * @return the ApiError object
     */
    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(
            javax.validation.ConstraintViolationException ex) {

        RestErrorResponse restErrorResponse = new RestErrorResponse(BAD_REQUEST);
        restErrorResponse.setMessage("Validation error");
        restErrorResponse.addValidationErrors(ex.getConstraintViolations());

        return buildResponseEntity(restErrorResponse);
    }

    /**
     * Handle Exception, handle generic Exception.class
     *
     * @param ex the Exception
     * @return the ApiError object
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex) {

        RestErrorResponse restErrorResponse = new RestErrorResponse(BAD_REQUEST);
        restErrorResponse.setMessage(String.format("The parameter '%s' of value '%s' could not be converted to type '%s'", ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName()));
        restErrorResponse.setDebugMessage(ex.getMessage());

        return buildResponseEntity(restErrorResponse);
    }

    @ExceptionHandler(AccessDeniedException.class)
    protected ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex) {

        RestErrorResponse restErrorResponse = new RestErrorResponse(UNAUTHORIZED);
        restErrorResponse.setMessage(ex.getMessage());
        restErrorResponse.setDebugMessage(ex.getMessage());

        return buildResponseEntity(restErrorResponse);
    }

    @ExceptionHandler(BadCredentialsException.class)
    protected ResponseEntity<Object> handleBadCredentialsException(BadCredentialsException ex) {

        RestErrorResponse restErrorResponse = new RestErrorResponse(BAD_REQUEST);
        restErrorResponse.setMessage(ex.getMessage());
        restErrorResponse.setDebugMessage(ex.getMessage());

        return buildResponseEntity(restErrorResponse);
    }

    @ExceptionHandler(RuntimeException.class)
    protected ResponseEntity<Object> handleRuntimeException(RuntimeException ex) {

        RestErrorResponse restErrorResponse = new RestErrorResponse(BAD_REQUEST);
        restErrorResponse.setMessage(ex.getMessage());
        restErrorResponse.setDebugMessage(ex.getMessage());

        return buildResponseEntity(restErrorResponse);
    }

    /**
     * Handle DataIntegrityViolationException, inspects the cause for different DB causes.
     * Like our unique constraints on the email in users table
     *
     * @param ex the DataIntegrityViolationException
     * @return the ApiError object
     */
    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<Object> handleDataIntegrityViolation(DataIntegrityViolationException ex) {

        if (ex.getCause() instanceof ConstraintViolationException) {
            return buildResponseEntity(new RestErrorResponse(HttpStatus.CONFLICT, "Database error", ex.getCause()));
        }
        return buildResponseEntity(new RestErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex));
    }

    private ResponseEntity<Object> buildResponseEntity(RestErrorResponse restErrorResponse) {
        return new ResponseEntity<>(restErrorResponse, restErrorResponse.getStatus());
    }

}