package me.helpinghand.helpinghand.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtFilter extends OncePerRequestFilter {

    private final JwtUtility jwtUtility;
    private final UserDetailsService userDetailsService;

    public JwtFilter(JwtUtility jwtUtility, UserDetailsService userDetailsService) {
        this.jwtUtility = jwtUtility;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String authorization = httpServletRequest.getHeader("Authorization");
        String token = null;
        String userName = null;

        // If authorization is not null and it starts with Bearer
        if(null != authorization && authorization.startsWith("Bearer ")) {
            // get token and username from the parsed token
            token = authorization.substring(7);
            userName = jwtUtility.getUsernameFromToken(token);
        }

        // If userName is not null, and we have not authenticated yet
        if(null != userName && SecurityContextHolder.getContext().getAuthentication() == null) {
            // get UserDetails from userDetailsService
            UserDetails userDetails = userDetailsService.loadUserByUsername(userName);

            // validate token and if token is not yet expired
            if(jwtUtility.validateToken(token,userDetails)) {

                // create authentication token instance
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                        = new UsernamePasswordAuthenticationToken(userDetails,
                        null, userDetails.getAuthorities());

                // set details
                usernamePasswordAuthenticationToken.setDetails(
                        new WebAuthenticationDetailsSource().buildDetails(httpServletRequest)
                );

                // set authentication in security context
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }

        }

        // next filter in the chain
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
