package me.helpinghand.helpinghand.repositories;

import me.helpinghand.helpinghand.models.Lender;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LenderRepository extends JpaRepository<Lender, Long> {
}
