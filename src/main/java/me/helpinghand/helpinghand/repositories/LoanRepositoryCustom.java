package me.helpinghand.helpinghand.repositories;

import me.helpinghand.helpinghand.models.Loan;

import java.math.BigDecimal;
import java.util.Set;

public interface LoanRepositoryCustom {
    Set<Loan> searchLoans(String name, String description, String requirements, BigDecimal maxTakeOut, BigDecimal netWorth);
}
