package me.helpinghand.helpinghand.repositories;

import me.helpinghand.helpinghand.models.Loan;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

public class LoanRepositoryCustomImpl implements LoanRepositoryCustom {

    private final EntityManager entityManager;

    public LoanRepositoryCustomImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Set<Loan> searchLoans(String name,
                                 String description,
                                 String requirements,
                                 BigDecimal maxTakeOut,
                                 BigDecimal netWorth) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Loan> query = builder.createQuery(Loan.class);
        Root<Loan> root = query.from(Loan.class);
        List<Predicate> predicates = new ArrayList<>();

        if(name != null) {
            predicates.add(builder.like(root.get("name"), "%" + name + "%"));
        }

        if (description != null) {
            predicates.add(builder.like(root.get("description"), "%" + description + "%"));
        }

        if (requirements != null) {
            predicates.add(builder.like(root.get("requirements"), "%" + requirements + "%"));
        }

        if (maxTakeOut != null) {
            predicates.add(builder.greaterThanOrEqualTo(root.get("maxTakeOutAmount"), maxTakeOut));
        }

        if(netWorth != null) {
            predicates.add(builder.lessThanOrEqualTo(root.get("minNetWorth"), netWorth));
        }

        predicates.add(builder.lessThanOrEqualTo(root.get("availableDate"), LocalDate.now()));
        predicates.add(builder.isTrue(root.get("published")));

        query.select(root).where(builder.and(predicates.toArray(new Predicate[0])));

        return new HashSet<>(entityManager.createQuery(query).getResultList());
    }
}
