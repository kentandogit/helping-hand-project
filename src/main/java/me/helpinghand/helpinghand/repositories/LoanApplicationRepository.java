package me.helpinghand.helpinghand.repositories;

import me.helpinghand.helpinghand.models.LoanApplication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface LoanApplicationRepository extends JpaRepository<LoanApplication, Long> {
    Set<LoanApplication> getByLoanId(Long loanId);
}