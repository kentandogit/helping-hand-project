package me.helpinghand.helpinghand.repositories;

import me.helpinghand.helpinghand.models.Borrower;
import me.helpinghand.helpinghand.models.Lender;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BorrowerRepository extends JpaRepository<Borrower, Long> {
}
