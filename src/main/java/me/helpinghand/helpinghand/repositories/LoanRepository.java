package me.helpinghand.helpinghand.repositories;

import me.helpinghand.helpinghand.models.Loan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoanRepository extends JpaRepository<Loan, Long>, LoanRepositoryCustom {
}
