package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.dto.BorrowerRequest;
import me.helpinghand.helpinghand.models.Borrower;
import me.helpinghand.helpinghand.models.User;
import me.helpinghand.helpinghand.repositories.BorrowerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class BorrowerServiceImplTest {

    private BorrowerServiceImpl borrowerService;
    private BorrowerRequest borrowerRequest;
    private Borrower borrower;

    @Mock
    private BorrowerRepository borrowerRepository;

    @Captor
    private ArgumentCaptor<Borrower> borrowerArgumentCaptor;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        borrowerService = new BorrowerServiceImpl(borrowerRepository, new BCryptPasswordEncoder());

        this.borrowerRequest = new BorrowerRequest();
        borrowerRequest.setEmail("borrower@borrower.com");
        borrowerRequest.setAddress("address");
        borrowerRequest.setDescription("description");
        borrowerRequest.setContactNumber("123456");

        User user = new User();
        user.setEmail(borrowerRequest.getEmail());

        this.borrower = new Borrower();
        borrower.setUser(user);
        borrower.setAddress(borrowerRequest.getAddress());
        borrower.setDescription(borrowerRequest.getDescription());
        borrower.setContactNumber(borrowerRequest.getContactNumber());
    }

    @Test
    void save() {

        when(borrowerRepository.save(borrowerArgumentCaptor.capture())).thenReturn(borrower);

        borrowerService.save(borrowerRequest);

        // verify save was called
        verify(borrowerRepository).save(borrower);

        // assert captured borrower
        Borrower capturedBorrower = borrowerArgumentCaptor.getValue();
        assertEquals(capturedBorrower.getUser().getEmail(), borrowerRequest.getEmail());
        assertEquals(capturedBorrower.getAddress(), borrowerRequest.getAddress());
        assertEquals(capturedBorrower.getDescription(), borrowerRequest.getDescription());
        assertEquals(capturedBorrower.getContactNumber(), borrowerRequest.getContactNumber());
    }

    @Test
    void update() {
        Long id = 1L;
        String description = "updated description";
        BigDecimal assets = BigDecimal.valueOf(100);

        borrower.setId(id);
        borrowerRequest.setDescription(description);
        borrowerRequest.setAssets(assets);

        when(borrowerRepository.findById(id)).thenReturn(Optional.of(borrower));
        when(borrowerRepository.save(borrowerArgumentCaptor.capture())).thenReturn(borrower);

        borrowerService.update(borrowerRequest, id);

        // verify save is called
        verify(borrowerRepository).save(borrower);

        // assert captured arguments
        Borrower capturedBorrower = borrowerArgumentCaptor.getValue();
        assertEquals(capturedBorrower.getUser().getEmail(), borrowerRequest.getEmail());
        assertEquals(capturedBorrower.getAddress(), borrowerRequest.getAddress());
        assertEquals(capturedBorrower.getContactNumber(), borrowerRequest.getContactNumber());
        assertEquals(capturedBorrower.getDescription(), description);
        assertEquals(capturedBorrower.getAssets(), assets);

    }

    @Test
    void findAll() {
        List<Borrower> borrowerList = new ArrayList<>();

        Borrower borrower = new Borrower();
        borrower.setId(1L);
        borrowerList.add(borrower);

        Borrower borrower2 = new Borrower();
        borrower.setId(2L);
        borrowerList.add(borrower2);

        when(borrowerRepository.findAll()).thenReturn(borrowerList);

        Set<Borrower> returnedBorrowers = borrowerService.findAll();
        assertEquals(returnedBorrowers.size(), 2);

        verify(borrowerRepository).findAll();
    }
}