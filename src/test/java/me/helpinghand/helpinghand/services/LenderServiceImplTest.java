package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.dto.LenderRequest;
import me.helpinghand.helpinghand.models.Lender;
import me.helpinghand.helpinghand.models.User;
import me.helpinghand.helpinghand.repositories.LenderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class LenderServiceImplTest {

    private LenderServiceImpl lenderService;
    private LenderRequest lenderRequest;
    private Lender lender;

    @Mock
    private LenderRepository lenderRepository;

    @Captor
    private ArgumentCaptor<Lender> lenderArgumentCaptor;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        lenderService = new LenderServiceImpl(lenderRepository, new BCryptPasswordEncoder());

        this.lenderRequest = new LenderRequest();
        lenderRequest.setEmail("borrower@borrower.com");
        lenderRequest.setAddress("address");
        lenderRequest.setDescription("description");
        lenderRequest.setContactNumber("123456");
        lenderRequest.setCompanyName("Lender Company");

        User user = new User();
        user.setEmail(lenderRequest.getEmail());

        this.lender = new Lender();
        lender.setUser(user);
        lender.setAddress(lenderRequest.getAddress());
        lender.setDescription(lenderRequest.getDescription());
        lender.setContactNumber(lenderRequest.getContactNumber());
        lender.setCompanyName(lenderRequest.getCompanyName());
    }

    @Test
    void save() {
        when(lenderRepository.save(lenderArgumentCaptor.capture())).thenReturn(lender);

        lenderService.save(lenderRequest);

        // verify save got called
        verify(lenderRepository).save(lender);

        // assert captured lender
        Lender capturedLender = lenderArgumentCaptor.getValue();
        assertEquals(capturedLender.getUser().getEmail(), lenderRequest.getEmail());
        assertEquals(capturedLender.getAddress(), lenderRequest.getAddress());
        assertEquals(capturedLender.getDescription(), lenderRequest.getDescription());
        assertEquals(capturedLender.getContactNumber(), lenderRequest.getContactNumber());
        assertEquals(capturedLender.getCompanyName(), lenderRequest.getCompanyName());
    }

    @Test
    void update() {
        Long id = 1L;
        String description = "updated description";
        String companyName = "Updated company name";

        lender.setId(id);
        lenderRequest.setDescription(description);
        lenderRequest.setCompanyName(companyName);

        when(lenderRepository.findById(id)).thenReturn(Optional.of(lender));
        when(lenderRepository.save(lenderArgumentCaptor.capture())).thenReturn(lender);

        lenderService.update(lenderRequest, id);

        // verify save is called
        verify(lenderRepository).save(lender);

        // assert captured arguments
        Lender capturedLender = lenderArgumentCaptor.getValue();
        assertEquals(capturedLender.getUser().getEmail(), lenderRequest.getEmail());
        assertEquals(capturedLender.getAddress(), lenderRequest.getAddress());
        assertEquals(capturedLender.getContactNumber(), lenderRequest.getContactNumber());
        assertEquals(capturedLender.getDescription(), description);
        assertEquals(capturedLender.getCompanyName(), companyName);
    }

    @Test
    void findAll() {
        List<Lender> lenderList = new ArrayList<>();

        Lender lender = new Lender();
        lender.setId(1L);
        lenderList.add(lender);

        Lender lender2 = new Lender();
        lender.setId(2L);
        lenderList.add(lender2);

        when(lenderRepository.findAll()).thenReturn(lenderList);

        Set<Lender> returnedLenders = lenderService.findAll();
        assertEquals(returnedLenders.size(), 2);

        verify(lenderRepository).findAll();
    }
}