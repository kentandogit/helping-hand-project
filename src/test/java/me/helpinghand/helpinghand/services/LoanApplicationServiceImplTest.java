package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.models.*;
import me.helpinghand.helpinghand.repositories.LoanApplicationRepository;
import me.helpinghand.helpinghand.repositories.LoanRepository;
import me.helpinghand.helpinghand.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class LoanApplicationServiceImplTest {

    private LoanApplicationServiceImpl loanApplicationService;

    @Captor
    ArgumentCaptor<LoanApplication> loanApplicationArgumentCaptor;

    @Mock
    private UserRepository userRepository;
    @Mock
    private LoanRepository loanRepository;
    @Mock
    private LoanApplicationRepository loanApplicationRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.loanApplicationService = new LoanApplicationServiceImpl(userRepository,
                loanRepository, loanApplicationRepository);

    }

    @Test
    void getApplications() {

        Long id = 1L;
        Set<LoanApplication> laSet = new HashSet<>();
        Loan loan = new Loan();
        loan.setId(id);

        LoanApplication la = new LoanApplication();
        la.setLoan(loan);
        laSet.add(la);

        LoanApplication la1 = new LoanApplication();
        la1.setLoan(loan);
        laSet.add(la1);

        when(loanApplicationRepository.getByLoanId(id)).thenReturn(laSet);

        Set<LoanApplication> resultSet = loanApplicationService.getApplications(id);

        // verify we are searching by id provided
        verify(loanApplicationRepository).getByLoanId(id);
        assertEquals(resultSet.size(), laSet.size());
    }

    @Test
    void applyThrow() {
        Long id = 1L;

        Loan loan = new Loan();
        loan.setAvailableDate(LocalDate.now().plusDays(5));

        when(userRepository.findByEmail(any())).thenReturn(Optional.of(new User()));
        when(loanRepository.findById(id)).thenReturn(Optional.of(loan));

        assertThrows(RuntimeException.class, () -> loanApplicationService.apply(id, "borrower@borrower.com"));
    }

    @Test
    void apply() {
        Long id = 1L;
        String username = "borrower@borrower.com";
        String description = "description";

        User user = new User();
        user.setEmail(username);

        Borrower borrower = new Borrower();
        borrower.setUser(user);
        user.setBorrower(borrower);

        Loan loan = new Loan();
        loan.setId(id);
        loan.setDescription(description);
        loan.setAvailableDate(LocalDate.now());

        when(userRepository.findByEmail(username)).thenReturn(Optional.of(user));
        when(loanRepository.findById(id)).thenReturn(Optional.of(loan));
        when(loanApplicationRepository.save(loanApplicationArgumentCaptor.capture())).thenReturn(new LoanApplication());

        loanApplicationService.apply(id, username);

        verify(loanApplicationRepository).save(any(LoanApplication.class));

        LoanApplication la = loanApplicationArgumentCaptor.getValue();
        assertEquals(la.getBorrower().getUser().getEmail(), username);
        assertEquals(la.getLoan().getDescription(), description);
        assertEquals(la.getLoan().getId(), id);
        assertEquals(la.getApplicationStatus(), ApplicationStatus.PENDING);
        assertEquals(la.getApplicationDate(), LocalDate.now());

    }

    @Test
    void updateStatus() {
        Long id = 1L;

        LoanApplication loanApplication = new LoanApplication();
        loanApplication.setId(id);
        loanApplication.setApplicationStatus(ApplicationStatus.PENDING);

        when(loanApplicationRepository.findById(id)).thenReturn(Optional.of(loanApplication));
        when(loanApplicationRepository.save(loanApplicationArgumentCaptor.capture())).thenReturn(loanApplication);

        loanApplicationService.updateStatus(id, ApplicationStatus.REVIEWING);
        LoanApplication la = loanApplicationArgumentCaptor.getValue();
        assertEquals(la.getApplicationStatus(), ApplicationStatus.REVIEWING);

        loanApplicationService.updateStatus(id, ApplicationStatus.REJECTED);
        LoanApplication la2 = loanApplicationArgumentCaptor.getValue();
        assertEquals(la2.getApplicationStatus(), ApplicationStatus.REJECTED);

        loanApplicationService.updateStatus(id, ApplicationStatus.APPROVED);
        LoanApplication la3 = loanApplicationArgumentCaptor.getValue();
        assertEquals(la3.getApplicationStatus(), ApplicationStatus.APPROVED);

        verify(loanApplicationRepository, times(3)).save(any(LoanApplication.class));
    }

    @Test
    void lenderOwnedApplication() {
        Long id = 1L;
        Long laId = 3L;
        String username = "borrower@borrower.com";
        String description = "description";

        User user = new User();
        user.setEmail(username);

        Lender lender = new Lender();
        lender.setId(id);
        lender.setUser(user);
        user.setLender(lender);

        Loan loan = new Loan();
        loan.setLender(lender);
        loan.setDescription(description);
        loan.setAvailableDate(LocalDate.now());

        LoanApplication la = new LoanApplication();
        la.setId(laId);
        la.setLoan(loan);

        when(userRepository.findByEmail(username)).thenReturn(Optional.of(user));
        when(loanApplicationRepository.findById(laId)).thenReturn(Optional.of(la));

        Boolean res = loanApplicationService.lenderOwnedApplication(laId, username);
        assertTrue(res);
    }

    @Test
    void borrowerAlreadyApplied() {
        Long id = 1L;
        String username = "borrower@borrower.com";
        String description = "description";

        User user = new User();
        user.setEmail(username);

        Borrower borrower = new Borrower();
        borrower.setUser(user);
        user.setBorrower(borrower);

        Loan loan = new Loan();
        loan.setId(id);
        loan.setDescription(description);
        loan.setAvailableDate(LocalDate.now());

        LoanApplication la = new LoanApplication();
        la.setLoan(loan);
        la.setBorrower(borrower);
        Set<LoanApplication> laSet = new HashSet<>();
        laSet.add(la);
        borrower.setLoanApplications(laSet);

        when(userRepository.findByEmail(username)).thenReturn(Optional.of(user));
        Boolean res = loanApplicationService.borrowerAlreadyApplied(id, username);
        assertTrue(res);
    }
}