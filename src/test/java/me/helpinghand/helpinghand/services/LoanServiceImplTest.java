package me.helpinghand.helpinghand.services;

import me.helpinghand.helpinghand.models.*;
import me.helpinghand.helpinghand.repositories.LoanRepository;
import me.helpinghand.helpinghand.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.access.AccessDeniedException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class LoanServiceImplTest {

    private LoanServiceImpl loanService;
    private Loan loan;
    private Lender lender;

    @Captor
    ArgumentCaptor<Loan> loanArgumentCaptor;

    @Mock
    private LoanRepository loanRepository;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        this.loanService = new LoanServiceImpl(loanRepository, userRepository);

        User user = new User();
        user.setEmail("lender@lender.com");
        user.setRole(Role.LENDER);

        this.lender = new Lender();
        lender.setCompanyName("Lender Company");
        user.setLender(lender);
        lender.setUser(user);

        this.loan = new Loan();
        loan.setLender(lender);
        loan.setAvailableDate(LocalDate.now());
        loan.setPublished(true);
        loan.setDescription("Description");
        loan.setRequirements("Requirements");
        loan.setName("Loan name");
        loan.setAllowSelfEmployed(true);
        loan.setMinNetWorth(BigDecimal.valueOf(100));
        loan.setMaxTakeOutAmount(BigDecimal.valueOf(1000000));
    }

    @Test
    void save() {
        String username = "lender@lender.com";
        String description = "Loan Description";
        String requirements = "Loan Requirements";
        BigDecimal minNetWorth = BigDecimal.valueOf(100);
        BigDecimal maxTakeOut = BigDecimal.valueOf(1000000);

        loan.setDescription(description);
        loan.setRequirements(requirements);
        loan.setMinNetWorth(minNetWorth);
        loan.setMaxTakeOutAmount(maxTakeOut);

        when(userRepository.findByEmail(username)).thenReturn(Optional.of(lender.getUser()));
        when(loanRepository.save(loanArgumentCaptor.capture())).thenReturn(loan);

        loanService.save(loan, username);

        verify(loanRepository).save(loan);

        Loan capturedLoan = loanArgumentCaptor.getValue();
        assertEquals(capturedLoan.getDescription(), description);
        assertEquals(capturedLoan.getRequirements(), requirements);
        assertEquals(capturedLoan.getMinNetWorth(), minNetWorth);
        assertEquals(capturedLoan.getMaxTakeOutAmount(), maxTakeOut);
    }

    @Test
    void update() {
        Long id = 1L;
        String description = "Loan Description";
        String requirements = "Loan Requirements";
        BigDecimal minNetWorth = BigDecimal.valueOf(100);
        BigDecimal maxTakeOut = BigDecimal.valueOf(1000000);

        loan.setId(id);
        loan.setDescription(description);
        loan.setRequirements(requirements);
        loan.setMinNetWorth(minNetWorth);
        loan.setMaxTakeOutAmount(maxTakeOut);

        when(loanRepository.findById(id)).thenReturn(Optional.of(loan));
        when(loanRepository.save(loanArgumentCaptor.capture())).thenReturn(loan);

        loanService.update(loan);

        verify(loanRepository).save(loan);

        Loan capturedLoan = loanArgumentCaptor.getValue();
        assertEquals(capturedLoan.getDescription(), description);
        assertEquals(capturedLoan.getRequirements(), requirements);
        assertEquals(capturedLoan.getMinNetWorth(), minNetWorth);
        assertEquals(capturedLoan.getMaxTakeOutAmount(), maxTakeOut);
    }

    @Test
    void lenderOwnsLoan() {
        Long id = 1L;
        String username = "lender@lender.com";
        lender.setId(id);

        when(userRepository.findByEmail(username)).thenReturn(Optional.of(lender.getUser()));
        when(loanRepository.findById(id)).thenReturn(Optional.of(loan));

        Boolean result = loanService.lenderOwnsLoan(id, username);
        assertTrue(result);
    }

    @Test
    void getLenderLoans() {
        String username = "lender@lender.com";
        Set<Loan> loanSet = new HashSet<>();

        Loan loan1 = new Loan();
        loan1.setId(1L);
        loan1.setLender(lender);

        Loan loan2 = new Loan();
        loan2.setId(2L);
        loan2.setLender(lender);

        lender.setLoans(loanSet);

        when(userRepository.findByEmail(username)).thenReturn(Optional.of(lender.getUser()));

        Set<Loan> loanSet1 = loanService.getLenderLoans(username);

        assertEquals(loanSet.size(), loanSet1.size());
    }

    @Test
    void searchLoanLender() {
        String username = "lender@lender.com";
        when(userRepository.findByEmail(username)).thenReturn(Optional.of(lender.getUser()));

        // showcasing assertThrows
        assertThrows(AccessDeniedException.class, () -> loanService.searchLoan(null, null, null, null, username));
    }

    @Test
    void searchLoanBorrower() {
        String username = "borrower@borrower.com";
        String name = "name";
        String description = "description";
        String requirements = "requirements";
        BigDecimal maxTakeOut = BigDecimal.valueOf(1000);

        User user = new User();
        user.setEmail(username);
        user.setRole(Role.BORROWER);

        Borrower borrower = new Borrower();
        borrower.setUser(user);
        user.setBorrower(borrower);
        borrower.setAssets(BigDecimal.valueOf(100));
        borrower.setLiabilities(BigDecimal.valueOf(50));

        when(userRepository.findByEmail(username)).thenReturn(Optional.of(borrower.getUser()));
        when(loanRepository.searchLoans(anyString(), anyString(), anyString(), any(), any())).thenReturn(new HashSet<>());

        loanService.searchLoan(name, description, requirements, maxTakeOut, username);

        // verify that our borrower got picked up and the service passed the borrowers networth
        // we should probably have another test for loan repository searchLoans to actually test the search
        verify(loanRepository).searchLoans(name, description, requirements, maxTakeOut, BigDecimal.valueOf(50));

    }
}